export let findGlassIndex = (dataGlasses, id) => {
  let index = 0;
  for (index; index < dataGlasses.length; index++) {
    if (dataGlasses[index].id == id) {
      return index;
    }
  }
};

export let saveGlass = (array, glass) => {
  if (array.length == 0) {
    array.push(glass);
    document.getElementById("button-before").style.opacity = "1";
  } else if (array.length == 1) {
    array.push(glass);
    document.getElementById("button-after").style.opacity = "1";
  } else if (array.length == 2) {
    array.splice(0, 1);
    array.push(glass);
    document.getElementById("button-before").style.opacity = "1";
    document.getElementById("button-after").style.opacity = "1";
  } else {
    return;
  }
};
