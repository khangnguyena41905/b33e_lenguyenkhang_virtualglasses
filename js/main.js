import { findGlassIndex, saveGlass } from "./controller.js";

let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];
let beforGlass = [];
let renderInfo = (glass) => {
  document.getElementById(
    "avatar"
  ).innerHTML = `<img src="${glass.virtualImg}" alt="" />`;
  let content = `
  <p>Brand: ${glass.brand}</p>
  <p>Name: ${glass.name}</p>
  <p>Color: ${glass.color}</p>
  <p>Price: ${glass.price}</p>
  <p>Description: ${glass.description}</p>
  `;
  document.getElementById("glassesInfo").style.display = "block";
  document.getElementById("glassesInfo").innerHTML = content;
};

let chooseGlass = (id) => {
  let index = findGlassIndex(dataGlasses, id);
  let currentGlass = dataGlasses[index];
  saveGlass(beforGlass, currentGlass);
  renderInfo(currentGlass);
};
window.chooseGlass = chooseGlass;

let renderGlasses = (dataGlasses) => {
  let contentHTML = "";
  dataGlasses.forEach((item) => {
    let content = `
    <div onclick="chooseGlass('${item.id}')" class="vglasses__items col-4">
    <img style="width: 100%" src="${item.src}" alt="" />
  </div>
        `;
    contentHTML += content;
  });
  document.getElementById("vglassesList").innerHTML = contentHTML;
};

renderGlasses(dataGlasses);

let removeGlasses = (bolean) => {
  chooseGlass(beforGlass[0].id);
  if (bolean == false) {
    document.getElementById("button-before").style.opacity = "0";
    document.getElementById("button-after").style.opacity = "1";
  } else {
    document.getElementById("button-before").style.opacity = "1";
    document.getElementById("button-after").style.opacity = "0";
  }
};
window.removeGlasses = removeGlasses;
